import socket
import sys
import requests
import requests_oauthlib
import json

# Replace the values below with yours
ACCESS_TOKEN = '95985191-0cmMRwwEukCSP8TYsuxtLQQGr2zPVpQGZ8w3Rp597'
ACCESS_SECRET = '5EZ2RVbKaQQ0ixA02sqFolMb0u0KOcuWbeyP6yzixucmf'
CONSUMER_KEY = 'GCo8GixsUDvS7hMpQYET1wZH5'
CONSUMER_SECRET = 'GwfFUEzUwiZIjTbJ7coEE5DNEfqhIidmCTIF0jOR8zkRGDsXXy'
my_auth = requests_oauthlib.OAuth1(CONSUMER_KEY, CONSUMER_SECRET,ACCESS_TOKEN, ACCESS_SECRET)


def send_tweets_to_spark(http_resp, tcp_connection):
    for line in http_resp.iter_lines():
        if line:
            try:
                full_tweet = json.loads(line)
                tweet_text = full_tweet['text'].encode('utf-8','ignore')
                print("Tweet Text: " + tweet_text)
                print ("------------------------------------------")
                tcp_connection.send(tweet_text + '\n')
            except:
                e = sys.exc_info()[0]
                print("Error: %s" % e)


def get_tweets():
    url = 'https://stream.twitter.com/1.1/statuses/filter.json'
    query_data = [('language', 'en'), ('locations', '-180,0,0,90'),('track','#')]
    #query_data = [('locations', '-130,-20,100,50'), ('track', '#')]
    query_url = url + '?' + '&'.join([str(t[0]) + '=' + str(t[1]) for t in query_data])
    response = requests.get(query_url, auth=my_auth, stream=True)
    print(query_url, response)
    return response


TCP_IP = "127.0.0.1"
TCP_PORT = 65432
conn = None
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)
print("Waiting for TCP connection...")
conn, addr = s.accept()
print("Connected... Starting getting tweets.")
resp = get_tweets()
send_tweets_to_spark(resp,conn)



