# TwitterStreamingApp

# TUTORIAIS USADOS
	
	FLASK
	https://linuxize.com/post/how-to-install-flask-on-ubuntu-18-04/
	
	

	RODAR APLICACAO
	###############

	cd /home/leandropaiva/spark-2.4.2-bin-hadoop2.6/bin

	./spark-submit /home/leandropaiva/TwitterStreaming/TwitterHttpClient/twitter_app.py
	./spark-submit /home/leandropaiva/TwitterStreaming/TwitterHttpClient/spark_app.py


	ABRIR O SPARK STREAMING UI-WEB
	http://127.0.0.1:4040



	ATIVAR O FLASK
	##############

	cd /home/leandropaiva/TwitterStreaming/my_flask_app

	source venv/bin/activate
	export FLASK_APP=app
	flask run

	http://127.0.0.1:5000/


	#COMPUTADOR REMOTO
	157.230.190.45:4040



	INSTALACAO DAS FERRAMENTAS PARA RODAR A APLICACAO
	#################################################

	Install JAVA DIRETO >> 

	sudo apt update
	apt install default-jre
	java -version

	hostname: ubuntu-s-1vcpu-2gb-nyc1-01

	Install SPARK HADOOP >> 

	cd /home
	mkdir /mbaspark
	cd /mbaspark
	tar -xzvf spark-2.4.2-bin-hadoop2.6.tgz
	wget https://archive.apache.org/dist/spark/spark-2.4.2/spark-2.4.2-bin-hadoop2.6.tgz 

	cd /home/mbaspark/spark-2.4.2-bin-hadoop2.6/bin

	Install Python 2
	sudo apt install python-minimal


	>> vim ~/.bashrc
	
	export SPARK_HOME=/home/mbaspark/spark-2.4.2-bin-hadoop2.6
	export PATH=$PATH:$SPARK_HOME/bin
	export PATH=$PATH:~/anaconda3/bin
	export PYTHONPATH=$SPARK_HOME/python:$PYTHONPATH
	#export PYSPARK_DRIVER_PYTHON="jupyter"
	#export PYSPARK_DRIVER_PYTHON_OPTS="notebook"
	export PYSPARK_PYTHON=python2
	export PATH=$PATH:$JAVA_HOME/jre/bin	

	>> source ~/.bashrc
	
	
	INSTALL PIP FOR PYTHON 2 WITH:

	>> sudo apt install python-pip	
	
	
	cd /home/mbaspark/spark-2.4.2-bin-hadoop2.6/bin

	./spark-submit /home/mbaspark/TwitterStreaming/TwitterHttpClient/twitter_app.py
	./spark-submit /home/mbaspark/TwitterStreaming/TwitterHttpClient/spark_app.py


	sudo pip3 install twython --upgrade	
